#include "ThreadPool.h"

using namespace std;

ThreadPool::ThreadPool(ushort maxThreadNum, uint timer_resolution)
        : mMaxThreadNumber(maxThreadNum), mLastEventID(0),
          mAcceptJobFlag(false), _timer(mIOService), _timer_resolution(timer_resolution),
          _cached_time(0)
{
}

ThreadPool::~ThreadPool()
{
   if (mAcceptJobFlag)
   {
      StopWorking();

      for (auto& t : mThreads)
      {
         delete t.second;
      }
   }

   if (!(mIOService.stopped()))
      mIOService.stop();
}

void ThreadPool::Run()
{
   for (ushort i = 0; i < mMaxThreadNumber; ++i)
   {
      auto t = new Thread(this);
      t->startThread();
      mThreads.insert({t->getOSID(), t});
   }
   mAcceptJobFlag = true;

   _time_start = std::chrono::steady_clock::now();
   timer_tick();
   mIOServiceThread = thread([this](){
      boost::asio::io_service::work work(mIOService);
      mIOService.run();
   });
}

void ThreadPool::timer_tick()
{
   _cached_time = ulong(std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - _time_start).count());
   _timer.expires_from_now(boost::posix_time::milliseconds(_timer_resolution));
   _timer.async_wait([this](const boost::system::error_code& error)
                     {
                        if (error != boost::asio::error::operation_aborted)
                           timer_tick();
                     });
}

void ThreadPool::RunOnAllThreads(std::function<void()> job)
{
   for (auto& t : mThreads)
   {
      promise<void> pr;
      future<void> re = pr.get_future();
      JobInfo ji {job, 0, move(pr)};
      t.second->reserveJob(std::move(ji));
      re.get();
   }
}

ulong ThreadPool::AddEvent(ulong jobGroup, std::function<void()> job, uint callAfter)
{
   auto timer = make_shared<boost::asio::deadline_timer>(mIOService, boost::posix_time::seconds(callAfter));
   //TODO: get rid of locks here
   mTimerLock.lock();
   ulong ind = ++mLastEventID;
   mTimersMap[ind] = timer;
   mTimerLock.unlock();
   timer->async_wait([jobGroup, job{std::move(job)}, ind, this](const boost::system::error_code& error)
                     {
                        if (likely(error != boost::asio::error::operation_aborted))
                        {
                           RemoveEvent(ind);
                           //TODO: balance the load between threads
                           auto tind = rand() % mThreads.size();
                           auto it = mThreads.begin();
                           std::advance(it, tind);
                           auto targetThread = it->first;
                           if (jobGroup)
                           {
                              mJobGroupLock.lock();
                              auto jit = mJobGroupMap.find(jobGroup);
                              if (jit == mJobGroupMap.end())
                              {
                                 mJobGroupMap.insert({jobGroup, targetThread});
                              }
                              else
                              {
                                 targetThread = jit->second;
                              }
                              mJobGroupLock.unlock();
                           }

                           mThreads[targetThread]->reserveJob(JobInfo {job, jobGroup, promise<void>()});
                           mEmptyQueueCV.notify_all(); //TODO: this is overkill!
                        }
                     });
   return ind;
}

void ThreadPool::RemoveEvent(ulong jobID)
{
   mTimerLock.lock();
   auto it = mTimersMap.find(jobID);
   if (likely(it != mTimersMap.end()))
   {
      it->second->cancel();
      mTimersMap.erase(it);
   }
   mTimerLock.unlock();
}

bool ThreadPool::GetNextJob(JobInfo& nextJob)
{
   auto id = std::this_thread::get_id();

   //TODO: get rid of locks here
   while (true)
   {
      if (likely(mJobQueue.try_dequeue(nextJob)))
      {
         ulong jg = nextJob.JobGroup;

         if (jg > 0)
         {
            mJobGroupLock.lock();
            auto jit = mJobGroupMap.find(jg);
            if (unlikely(jit == mJobGroupMap.end()))
            {
               mJobGroupMap.insert({jg, id});
            }
            else if (likely(jit->second != id))
            {
               mThreads[jit->second]->reserveJob(move(nextJob));
               mEmptyQueueCV.notify_all(); //TODO: this is overkill!
               mJobGroupLock.unlock();
               continue;
            }
            mJobGroupLock.unlock();
         }
         return true;
      }

      return false;
   }
}

future<void> ThreadPool::AddJob(ulong jobGroup, std::function<void()> job)
{
   if (mAcceptJobFlag)
   {
      promise<void> pr;
      future<void> re = pr.get_future();
      JobInfo ji {move(job), jobGroup, move(pr)};
      if (jobGroup > 0)
      {
         mJobGroupLock.lock();
         auto jit = mJobGroupMap.find(jobGroup);
         if (likely(jit != mJobGroupMap.end()))
         {
            mThreads[jit->second]->reserveJob(move(ji));
            mEmptyQueueCV.notify_all(); //TODO: this is overkill!
            mJobGroupLock.unlock();
            return std::move(re);
         }
         mJobGroupLock.unlock();
      }
      mJobQueue.enqueue(move(ji));
      mEmptyQueueCV.notify_one();
      return std::move(re);
   }

   return future<void>();
}

void ThreadPool::StopWorking()
{
   mAcceptJobFlag = false;
   while (mJobQueue.size_approx() > 0)
   {
      this_thread::sleep_for(chrono::seconds(1));
   }

//   while (mTimersMap.size() > 0)
//   {
//      this_thread::sleep_for(chrono::seconds(1));
//   }

   for (auto t : mThreads)
   {
      t.second->stopWorking(mEmptyQueueCV);
   }

   if (!(mIOService.stopped()))
      mIOService.stop();

   if (mIOServiceThread.joinable())
      mIOServiceThread.join();
}

void ThreadPool::jobGroupFinished(ulong jid)
{
   mJobGroupLock.lock();
   mJobGroupMap.erase(jid);
   mJobGroupLock.unlock();
}

