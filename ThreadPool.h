#pragma once

#include <list>
#include <mutex>
#include <thread>
#include <condition_variable>
#include <future>
#include <iostream>
#include <map>
#include <unordered_map>
#include "Thread.h"
#include <boost/asio/deadline_timer.hpp>

class ThreadPool
{
   typedef std::shared_ptr<boost::asio::deadline_timer> timerPointer;

public:

   ThreadPool(ushort maxThreadNum = 2, uint timer_resolution = 1);

   ~ThreadPool();

   ulong AddEvent(ulong jobGroup, std::function<void()> job, uint callAfter);
   ulong AddEvent(std::function<void()> job, uint callAfter)
   {
      return AddEvent(ulong(0), job, callAfter);
   }

   void RemoveEvent(ulong jobID);

   void RunOnAllThreads(std::function<void()> job);

   std::future<void> AddJob(ulong jobGroup, std::function<void()> job);

   std::future<void> AddJob(std::function<void()> job)
   {
      return AddJob(ulong(0), job);
   }

   template<class... InpTypes, class... ArgTypes>
   std::future<void> AddJob(ulong jobGroup, std::function<void(InpTypes...)> job, ArgTypes... args)
   {
      return AddJob(jobGroup, std::bind(job, args...));
   }

   template<class... InpTypes, class... ArgTypes>
   std::future<void> AddJob(std::function<void(InpTypes...)> job, ArgTypes... args)
   {
      return AddJob(ulong(0), job, args...);
   }

   template<class... InpTypes, class... ArgTypes>
   std::future<void> AddJob(ulong jobGroup, void(* job)(InpTypes...), ArgTypes... args)
   {
      std::function<void(InpTypes...)> ff = job;
      return AddJob(jobGroup, ff, args...);
   }

   template<class... InpTypes, class... ArgTypes>
   std::future<void> AddJob(void(* job)(InpTypes...), ArgTypes... args)
   {
      return AddJob(ulong(0), job, args...);
   }

   template<class T, class... InpTypes, class... ArgTypes>
   std::future<void> AddJob(ulong jobGroup, void(T::* job)(InpTypes...), T* cl, ArgTypes... args)
   {
      return AddJob(jobGroup, std::bind(job, cl, args...));
   }

   template<class T, class... InpTypes, class... ArgTypes>
   std::future<void> AddJob(void(T::* job)(InpTypes...), T* cl, ArgTypes... args)
   {
      return AddJob(ulong(0), job, cl, args...);
   }

   void WaitFor(std::future<void>&& waitingFuture)
   {
      for (auto it : mThreads)
      {
         if (it.second->getOSID() == std::this_thread::get_id())
         {
            it.second->continueTillFuture(std::move(waitingFuture));
            break;
         }
      }
   }

   void Run();

   bool GetNextJob(JobInfo& nextJob);
   void StopWorking();
   void jobGroupFinished(ulong jid);
   size_t getPendingJobsCount()
   {
      size_t c = mJobQueue.size_approx();
      for (auto& t : mThreads)
         c += t.second->getReservedJobsCount();
      return c;
   }

   void WaitForNewJobs(std::unique_lock<std::mutex>& lck)
   {
      //TODO: 5 milliseconds is good??
      mEmptyQueueCV.wait_for(lck, std::chrono::milliseconds(5));
   }

   std::mutex& GetEmptyQueueMutex()
   {
      return mEmptyQueueMutex;
   }

   bool isStarted()
   {
      return mAcceptJobFlag;
   }

   ulong getTime()
   {
      return _cached_time;
   }

private:
   moodycamel::BlockingConcurrentQueue<JobInfo> mJobQueue; //TODO: manage producer/consumer tokens
   ushort mMaxThreadNumber;
   ulong mLastEventID;
   bool mAcceptJobFlag;
   std::unordered_map<std::thread::id, Thread*> mThreads;
   std::unordered_map<ulong, std::thread::id> mJobGroupMap;
   std::map<std::pair<std::thread::id, uint>, void*> mExternalThreadResources;
   std::recursive_mutex mJobGroupLock;
   std::mutex mEmptyQueueMutex;
   std::condition_variable mEmptyQueueCV;
   std::unordered_map<ulong, timerPointer> mTimersMap;
   std::recursive_mutex mTimerLock;
   boost::asio::io_service mIOService;
   std::thread mIOServiceThread;

   boost::asio::deadline_timer _timer;
   uint _timer_resolution;
   std::atomic<ulong> _cached_time;
   std::chrono::time_point<std::chrono::steady_clock, std::chrono::nanoseconds> _time_start;

   void timer_tick();
};