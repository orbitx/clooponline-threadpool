#include <iostream>
#include <ThreadPool.h>

using namespace std;

ThreadPool tp(8);
time_t lastlog = time(0);

void forLoop()
{
   static atomic<bool> lock;

   if(lock)
   {
      throw runtime_error("function called but it is locked!");
   }

   lock = true;
   this_thread::sleep_for(chrono::milliseconds(500));

   cout << "Function called" << endl;
   lock = false;
}

int main()
{
   thread t(&ThreadPool::Run, &tp);
   this_thread::sleep_for(chrono::seconds(1));
   chrono::high_resolution_clock::time_point start = chrono::high_resolution_clock::now();

   for (uint i = 0; i < 10; ++i)
   {
      tp.AddJob(forLoop).get();
   }

   tp.StopWorking();
   if (t.joinable())
      t.join();
   chrono::high_resolution_clock::time_point end = chrono::high_resolution_clock::now();
   chrono::duration<double> time_span = chrono::duration_cast<chrono::duration<double>>(end - start);
   cout << "time: " << time_span.count() << endl;

   return 0;
}