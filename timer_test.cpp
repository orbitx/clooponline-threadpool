#include <iostream>
#include <ThreadPool.h>

using namespace std;

ThreadPool tp(8);

int main()
{
   tp.Run();
   ulong a = tp.getTime();
   this_thread::sleep_for(chrono::milliseconds(100));
   ulong b = tp.getTime();
   this_thread::sleep_for(chrono::milliseconds(10));
   ulong c = tp.getTime();

   cout << b - a << endl;
   cout << c - b << endl;

   return 0;
}