#include <iostream>
#include <ThreadPool.h>

using namespace std;

ulong fid;
ThreadPool tp(8);
uint i = 0;

void F()
{
   if (i < 10)
   {
      cout << "F has been called!" << endl;
      throw runtime_error("function F has been called");
   }
}

void G()
{
   cout << "G called, i = " << i << endl;
   if (i < 10)
   {
      tp.RemoveEvent(fid);
      fid = tp.AddEvent(0, F, 2);
      tp.AddEvent(1, G, 1);
      ++i;
   }

   static auto id = this_thread::get_id();
   static bool firstVisit = true;
   if (firstVisit)
   {
      cout << "Function G has been called from thread: " << id << endl;
      firstVisit = false;
   }

   if (id != this_thread::get_id())
   {
      cout << "function G has been run in another thread: " << id << endl;
      throw runtime_error("function G has been run in multiple threads");
   }
}

int main()
{
   tp.Run();
   fid = tp.AddEvent(0, F, 2);
   tp.AddEvent(1, G, 1);

   while(true)
      this_thread::sleep_for(chrono::seconds(1));

   tp.StopWorking();
   return 0;
}