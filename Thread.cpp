#include "Thread.h"
#include "ThreadPool.h"

using namespace std;

Thread::Thread(ThreadPool* tp)
        : mContinueFlag(false), mThreadPool(tp)
{

}

Thread::~Thread()
{
}

void Thread::run(std::future<void>&& waitingFuture)
{
   if (!waitingFuture.valid())
   {
      mOSID = this_thread::get_id();
      mContinueFlag = true;
   }

   while (mContinueFlag)
   {
      if (unlikely(waitingFuture.valid() && waitingFuture.wait_for(std::chrono::milliseconds(0)) == future_status::ready))
      {
         return;
      }

      JobInfo nj;
      bool isJobReady = false;
      if (likely(mReservedJobs.try_dequeue(nj)))
      {
         isJobReady = true;
      }
      else
      {
         isJobReady = mThreadPool->GetNextJob(nj);
      }

      if (likely(isJobReady))
      {
         try
         {
            nj.Callable();
            nj.JobPromise.set_value();
         }
         catch(...)
         {
            nj.JobPromise.set_exception(current_exception());
         }
      }
      else if (mReservedJobs.size_approx() == 0)
      {
         unique_lock<mutex> ulk(mThreadPool->GetEmptyQueueMutex());
         mThreadPool->WaitForNewJobs(ulk);
      }
   }
}

void Thread::startThread()
{
   mFuture = async(launch::async, &Thread::run, this, future<void>());

   while (mOSID == std::thread::id(0))
      ;
}

void Thread::stopWorking(condition_variable& emptyQueueCV)
{
   if (mContinueFlag)
   {
      mContinueFlag = false;
      while (mFuture.wait_for(chrono::seconds(0)) != future_status::ready)
      {
         emptyQueueCV.notify_one();
      }
      mFuture.get();
   }
}

