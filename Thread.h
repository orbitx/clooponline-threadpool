#pragma once

#include "concorrentqueue/blockingconcurrentqueue.h"
#include <functional>
#include <mutex>
#include <future>
#include <unordered_map>

#define likely(x)       __builtin_expect((x),1)
#define unlikely(x)     __builtin_expect((x),0)

struct JobInfo
{
   std::function<void()> Callable;
   ulong JobGroup;
   std::promise<void> JobPromise;
};

class ThreadPool;

class Thread
{
public:
   Thread(ThreadPool* tp);
   ~Thread();

   void startThread();
   void reserveJob(JobInfo&& job) { mReservedJobs.enqueue(std::move(job)); }
   void stopWorking(std::condition_variable& emptyQueueCV);
   void continueTillFuture(std::future<void>&& waitingFuture) { run(std::move(waitingFuture)); }
   size_t getReservedJobsCount() { return mReservedJobs.size_approx(); }
   std::thread::id getOSID() { return mOSID; }

private:
   void run(std::future<void>&& waitingFuture);

   bool mContinueFlag;
   ThreadPool* mThreadPool;
   std::future<void> mFuture;
   moodycamel::BlockingConcurrentQueue<JobInfo> mReservedJobs;
   std::thread::id mOSID;
};