#include <iostream>
#include <ThreadPool.h>

using namespace std;

time_t lastlog;
bool isPrime(int n)
{
   static int meet = 0;
   static bool logged = false;
   ++meet;

   if (lastlog + 1 == time(0))
   {
      logged = false;
   }

   if (lastlog + 2 == time(0) && !logged)
   {
      lastlog = time(0);
      cout << meet << endl;
      logged = true;
   }

   if (n == 2) return false;
   if (n % 2 == 0) return false;
   for (int i = 3; 3 * i <= n; i += 2)
      if (n % i == 0) return false;
   return true;
}

void primeWrapper(int n)
{
   isPrime(n);
}

int main()
{
   srand(time(0));

   ThreadPool tp(8);
   tp.Run();

   for (int i = 500000; i <= 1000000; i++)
   {
      tp.AddJob(&primeWrapper, i);
   }
   cout << "add completed" << endl;

   chrono::high_resolution_clock::time_point start = chrono::high_resolution_clock::now();
   lastlog = time(0);

   tp.StopWorking();

   chrono::high_resolution_clock::time_point end = chrono::high_resolution_clock::now();
   chrono::duration<double> time_span = chrono::duration_cast<chrono::duration<double>>(end - start);
   cout << "time: " << time_span.count() << endl;

   return 0;
}