#include <iostream>
#include <ThreadPool.h>

using namespace std;

thread_local int* intptr;
ThreadPool tp(8);

void F()
{
   cout << this_thread::get_id() << ": " << &intptr << endl;
}

int main()
{
   tp.Run();
   tp.RunOnAllThreads(F);
   tp.StopWorking();
   return 0;
}