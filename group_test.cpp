#include <iostream>
#include <ThreadPool.h>

using namespace std;

void F()
{
   static auto id = this_thread::get_id();
   static bool firstVisit = true;
   if (firstVisit)
   {
      cout << "Function F has been called from thread: " << id << endl;
      firstVisit = false;
   }

   if (this_thread::get_id() != id)
   {
      cout << "function F has been run in another thread: " << id << endl;
      throw runtime_error("function F has been run in multiple threads");
   }

   for (ulong i = 0; i < 100000000; ++i)
      ;
}

void G()
{
   static auto id = this_thread::get_id();
   static bool firstVisit = true;
   if (firstVisit)
   {
      cout << "Function G has been called from thread: " << id << endl;
      firstVisit = false;
   }

   if (this_thread::get_id() != id)
   {
      cout << "function G has been run in another thread: " << id << endl;
      throw runtime_error("function G has been run in multiple threads");
   }

   for (ulong i = 0; i < 100000000; ++i)
      ;
}

void H()
{
   static auto id = this_thread::get_id();
   static bool firstVisit = true;
   if (firstVisit)
   {
      cout << "Function H has been called from thread: " << id << endl;
      firstVisit = false;
   }

   if (this_thread::get_id() != id)
   {
      cout << "function H has been run in another thread: " << id << endl;
      throw runtime_error("function H has been run in multiple threads");
   }

   for (ulong i = 0; i < 100000000; ++i)
      ;
}

int main()
{
   ThreadPool tp(8);

   for (int i = 1; i <= 200000; i++)
   {
      tp.AddJob(1, &F);
      tp.AddJob(2, &G);
      tp.AddJob(3, &H);
   }
   cout << "add completed" << endl;

   tp.Run();
   tp.StopWorking();
   cout << "calling finished" << endl;

   return 0;
}
